---
title: About me
subtitle: راجع به من 
comments: false
bigimg: [{src: "/img/triangle-thumb.jpg"}]
---
{{< gallery caption-effect="fade" >}}
  {{< figure  link="/img/photo_2018-01-13_02-03-47.jpg" caption="Selfi" alt="1396/10/20 - Partvanak Travel Agency" >}}
{{< figure  link="/img/images.duckduckgo.com.jpg" caption="Selfi"  alt="1395/1/5 - kooh Sangi" >}}
  {{< figure  link="/img/photo_2018-01-13_02-10-38.jpg" caption="Selfi" alt="1394/12/6 - Partvanak Travel Agency" >}} 
{{< /gallery >}}
______
<p dir="rtl">
 سروش افضلیان هستم و در مقطع کاردانی رشته کامپیوتر، شاخه نرم افزار در دانشگاه شاندیز ادامه تحصیل میدهم و 
علاقه زیادی به :</br>
- آی تی (فناوری اطلاعات) |‌ (IT (Information Technology    </br>
- فدورا گنو/لینوکس |‌ Fedora Gnu/Linux</br>
- ‌بی ام دبلیو |‌ BMW </br>- ...</br>
 دارم. </br></br>یک سال بصورت رسمی به عنوان یک (SysAdmin) در ۳ شرکت : </br>
-  شرکت حمل و نقل بین الملل پی بان توس </br>
-  شرکت حمل و نقل بین الملل کهکشان گردان </br>
-  آژانس هواپیمایی پارت ونک </br>
 بصورت همزمان مشغول به کار بودم و به دلیل کنکور سال 95 از کار استفاء دادم.
________
{{< gallery caption-effect="fade" >}}
  {{< figure  link="/img/Screenshot from 2018-01-11 10-43-34.png" caption="Fedora27" >}}
  {{< figure  link="/img/12345.png" caption="Fedora26" alt="Desktop Screenshot" >}}
  {{< figure  link="/img/photo_2018-01-13_02-10-54.jpg" caption="فدورا۲۴" alt="Desktop Screenshot" >}}
    {{< figure  link="/img/triangle-thumb.jpg" caption="پارت ونک" alt="Workplace" >}}
{{< figure  link="/img/Screenshot from 2018-01-22 01-24-34.png" caption="Fedora"  alt="Fedora27" >}}
  {{< figure  link="/img/photo_2018-01-13_02-11-35.jpg" caption="پارت ونک" alt="Workplace" >}} 
{{< /gallery >}}
