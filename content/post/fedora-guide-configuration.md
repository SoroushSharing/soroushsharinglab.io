---
title: "Fedora Guide"
subtitle: "#2 First Configuration"
date: 2018-01-06T00:00:04+03:30
tags: ["fedora", "configuration","first", "linux", "Gnu/linux", "لینوکس", "فدورا", "گنولینوکس"]
bigimg: [{src: "/img/fedoracover.jpg", desc: "Get Fedora"}, {src: "/img/images.duckduckgo.com.png", desc: "Fedora GnuLinux"}]
---
<p dir="rtl" >
🔅 `فدورا` و دیگر توزیع‌های گنولینوکس، از ساختار منظم با ویژگی‌های بسیار ظریف و دقیقی تشکیل شده‌اند، این ساختار شگفت‌انگیز به دلیل وجود `هسته` یا `کرنلی ` متن‌باز به نام `لینوکس` در دل آنهاست که این هسته در کنار `نرم‌افزارهای آزاد`(پروژه گنو‍) با نام گنولینوکس خوانده می‌شود!

<!--more-->
<p dir="rtl" >
🔅 `فدورا گنولینوکس` پس از نصب، تا حد زیادی، نیاز به `Device Driver` برای شناخت چیپ ست‌ها و قطعات سخت‌افزاری کامپیوتر و یا لپ‌تاپ شما ندارد و این بخاطر وجود کرنلی با هزاران خط کد است که از آن انتظار می رود سخت‌افزار سیستم شما رو شناسایی و در نهایت پشتیبانی کند.<br>
🔅 این سیستم‌عامل از مخازن نرم‌افزاری `Repository` برای `اپدیت` و `نصب` نرم‌افزارهای مورد نیاز که `متن‌باز` و `رایگان` هستند، بصورت آنلاین استفاده می‌کند.<br>
نگهداری این بسته های نرم‌افزاری و امنیتی در سرور های مختلف و در کشورهای زیادی انجام می شود.این پروسه باعث می‌شود روند نصب نرم‌افزارها خیلی سریع‌تر و مطمئن‌تر باشد تا اینکه کاربر بخواهد نرم‌افزارهای مورد نیازش را از سایت‌ها و منابعی که هیچ دلیلی بر `اعتماد`  به  آنها نیست دانلود و نصب کند.
_____
<p dir="rtl" >
🔅 تا الان با [نحوه نصب فدورا ](https://soroushsharing.gitlab.io/post/fedora-quide-start-installation/) آشنا شدید و باید بدانید بعد از نصب باید یکسری از کارهای اولیه رو انجام بدید.

### 🔅 First Configuration<br><br>
#### 0. Addnig UserName as Administrator
> 
<p dir="rtl">  
در مرحله `Configuration` [نصب سیستم‌عامل فدورا](https://soroushsharing.gitlab.io/post/fedora-quide-start-installation/) گفته شد تا تیک چک‌باکس<br> `Make this user Administrator ` رو بزنید.<br>دلیل این‌کار این بود که در هنگام استفاده از دستور `sudo` دچار چنین مشکلی >><br> `User is not in the sudoers file. This incident will be reported ` نشوید!<br>



<p dir="rtl">
اگر فراموش کردید باید دستورات زیر رو به‌دقت طی کنید!
<p dir="ltr"> 
```
su -c "usermod -aG username root" 
```
<p dir="rtl"> 
و به‌جای عبارت `username`، نام کاربری‌ای که درهنگام نصب ایجاد کردید رو وارد کنید!<br>
روش دوم ساختن `alias`  برای نام‌های کاربریه!
جهت اعمال پیکربندی فایل visudo  رو با دسترسی ریشه باز کرده و تغییرات زیر رو اعمال می‌کنیم:
```
sudo visudo	
```
<p dir="rtl">
الان شما درحال تغییر فایل متنی visudo  هستید.پس برای ایحاد تغییرات کلید `i` رو بزنید و دستور زیر رو وارد کنید:
```
User_Alias ADMINS = user
ADMINS ALL=(ALL) ALL
```
<p dir="rtl">
بجای `user`، نام کاربری‌ای که درهنگام نصب ایجاد کردید رو وارد کنید!<br>حالا کلید esc رو زده و کلیدهای `wq:` بزنید تا فایل متنی ذخیره و بسته شود.<br>الان می‌تونید بدون وجود مشکل از sudo استفاده کنید .

#### 1. Addnig RPM Fusion Repository
<p dir="rtl"> 
یکی از مخازنی که استفاده زیادی برای `کاربران خانگی` Fedora دارد، مخازن `RPM Fusion` می‌باشد. این مخازن شامل انواع `کدک‌های صوتی و تصویری`، انواع `درایورهای سخت‌افزاری`، انواع نرم‌افزارهای `پخش کننده فایل های مولتی مدیا` و بسیاری نرم‌افزار دیگر می باشد.<br>
جهت نصب مخازن `RPM Fusion` در فدورا کافیه تا دستورات زیر رو داخل ترمینال وارد کنید :<br>
<p dir="ltr"> 
```
sudo -i
dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
dnf -y install gstreamer* gstreamer-ffmpeg gstreamer-plugins-bad gstreamer-plugins-bad-free gstreamer-plugins-bad-nonfree gstreamer-plugins-base gstreamer-plugins-good gstreamer-plugins-ugly gstreamer-plugins-bad ffmpeg libdvdread libdvdnav audacious-plugins-freeworld-mp3 phonon-backend-gstreamer xine-lib-extras-freeworld libva-intel-driver libva-intel-hybrid-driver python3-tkinter 
```
![img](/img/Screenshot from 2018-01-03 09-29-17.png "RPM Fusion Repository")

#### 2. First Update‌ & Upgrade
<p dir="rtl"> 
اولین اپدیت فدورا `ضروریه` چون شامل بروزرسانی مخازن، کرنل، کتابخانه‌ها، ماژول‌ها، درایورها، وتمام بسته‌های امنیتی و نرم‌افزاری برای سیستم‌عامل شما میشه. 
<p dir="ltr"> 
```
dnf update
```
![img](/img/Screenshot from 2018-01-03 09-31-38.png "Update & Upgrade")

#### 3.  Gnome Tweak Tool
<p dir="rtl"> 
نرم‌افزار `tweak-tool` ابزاری برای پیکربندی یا تنظیم یکسری از تغییرات دلخواه مثل تغیییر تم،ایکون‌ها و ...  در دسکتاپ گنوم می باشد.
<p dir="ltr"> 
```
dnf install gnome-tweak-tool
```
![img](/img/Screenshot from 2018-01-03 10-06-23.png "Gnome Tweak Tool")

#### 4. Gnome Shell Extensions
<p dir="rtl"> 
نصب و فعال‌سازی شل‌های گنوم.
بعد از اجرای دستور `dnf install chrome-gnome-shell ` و فعال‌سازی شل‌ها(طبق اسکرین‌شات زیر)‌.<br>
شل `dash to dock ` رو جستجو کرده و نصبش کنید.
<p dir="ltr"> 
```
dnf install chrome-gnome-shell 
```
![img](/img/Screenshot from 2018-01-03 10-09-28.png "Chrome Gnome Shell ")

#### 5. Software‍ Center
<p dir="rtl"> 
نیازی به نصب این نرم‌افزار نیست و با یه جستجو ساده در نرم‌افزاراتون پیداش می‌کنید.

![img](/img/Screenshot from 2018-01-03 10-16-03.png "Software Center")

#### 6. Chromium
<p dir="rtl"> 
نصب مرورگر متن باز `کرومیوم`.
```
dnf install chromium
```
![img](/img/Screenshot from 2018-01-03 10-39-38.png "Chromium")

#### 7. Nano 
<p dir="rtl"> 
نصب ویرایشگر متن  `nano`.
```
dnf install nano
```
![img](/img/Screenshot from 2018-01-03 10-20-09.png "NANO")
 
#### 8. Arc theme & Papirus icon theme 
<p dir="rtl"> 
نصب `Arc Theme`و `Papirus icon theme`. <br>
برای تعویض تم و ایکون پک فدورا نرم‌افزار `Gnome-tweak-tool` رو اجرا کنید و به این ادرس ` Appearance>> theme` برید.
<p dir="ltr"> 
```
dnf install arc-theme 
dnf copr enable dirkdavidis/papirus-icon-theme
dnf install papirus-icon-theme
```
![img](/img/Screenshot from 2018-01-03 11-32-56.png "Arc theme & Papirus icon theme")


#### 9. Neofetch
<p dir="rtl">
نمایش مشخصات سیستم.
```
dnf install neofetch 
```
![img](/img/Screenshot from 2018-01-03 11-45-12.png "Neofetch")
_________
##### 🔅 Read next `Fedora GNU/Linux` article step `#3` [TOR Configuration](https://soroushsharing.gitlab.io/post/fedora-guide-tor/).

____
<div id="graphcomment"></div>


