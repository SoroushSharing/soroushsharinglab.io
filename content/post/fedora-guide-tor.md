---
title: "Fedora Guide"
subtitle: "#3 TOR Configuration "
date: 2018-01-15T08:22:45+03:30
tags: ["tor" , "fedora" , "torconfiguration" , "proxy"]
bigimg: [{src: "/img/torcover2.png"}, {src: "/img/fedoracover.jpg", desc: "Get Fedora"}]
---
<p dir="rtl">
‍🔅 ` تور` ‍‍ یک پروژه متن‌باز و رایگان که نمونه‌ای از مدل شبکه پیازی `Onion Network` است و با هدف ناشناس ماندن و آزادی هرچه بیشتر کاربران در اینترنت، بوجود آمد. <br>
 در ادامه با جزییات این `مسیریابی پیازی` در شبکه تور و چگونگی نصب و پیکربندی تور در `فدورا` آشنا می‌شویم.

<!--more-->
### 🔅 TOR Introduction
#### 🔅 Anonymity Network 
<div id="15153969742997286"><script type="text/JavaScript" src="https://www.aparat.com/embed/bc251?data[rnddiv]=15153969742997286&data[responsive]=yes"></script></div>

### 🔅 Installation TOR 
<p dir="rtl"> 
 جهت نصب کلاینت تور در فدورا کافیه یک ترمینال باز کرده و دستورات زیر رو بترتیب وارد کنید:
```
sudo -i 
dnf install tor 
```

<p dir="rtl"> 
برای فعال کردن تور باید سرویسش رو فعال کنید:<br>
🔅 بعد از نصب و اجرا، در اولین قدم تور لیست سرورها`گره‌ها`ی خودش رو از یه ادرس در سرور اصلی تور دریافت و ذخیره می‌کنه.در اینجا تور ادرس سرورها و کلیدها‌یی رو که عمومی هستن رو دریافت می‌کنه .

```
systemctl start tor.service
```
<p dir="rtl"> 
خب بعد از فعال کردن سرویس کلاینت تور برای مطمئن شدن از وضعیتش باید دستور زیر رو اجرا کنید:
```
systemctl status tor.service
```
  <link rel="stylesheet" type="text/css" href="/css/asciinema-player.css" />

  <asciinema-player src="/json/tor.json" cols="107" rows="35"></asciinema-player>
  <script src="/js/asciinema-player.js"></script>
<p dir="rtl"> 
خب همونطور که می بینیم 🙄 تور` ۱۰۰٪` شده و آمادس.<br>
اگه به ۱۰۰٪ نرسید کمی صبر کنید دوباره کامند `systemctl status tor.service` رو چند بار اجرا کنید (برای خارج شدن از این وضعیت کلید `q `یا` CTRL + C` رو بزنید.) تا مطمئن بشوید که باکمی تاخیر باز هم متصل نشد و بین ۵٪ تا ۸۵٪ مانده‌است.پس باید `پیکربندی پل‌ها` رو انجام بدید.

###  🔅 Installation obfs4proxy
<p dir="rtl">
جهت پیکربندی پل ها در فدورا اول باید بسته` obfs4proxy `رو نصب کنید :
```
dnf install obfs4
```
<br>
### 🔅 Transport SELinux
> 
<p dir="rtl">
بعد از نصب ` obfs4proxy ` نیازه تا امکان اجرای این بسته رو با برچسبی که در تور دارد رو فعال کنیم.
دقت کنید، در خیلی از آموزش‌ها دیدم که نوشته‌اند `SELinux `رو بطور کلی غیرفعال کنید که اصلا راه‌حل درستی نیست.





<p dir="rtl">
=======
<p dir="rtl">
بعد از نصب ` obfs4proxy ` نیازه تا امکان اجرای این بسته رو با برچسبی که در تور دارد رو فعال کنیم.
دقت کنید، در خیلی از آموزش‌ها دیدم که نوشته‌اند `SELinux `رو بطور کلی غیرفعال کنید که اصلا راه‌حل درستی نیست.





<p dir="rtl">
از طریق بسته‌ی `  tor-exec-transport-selinux ` توی مخازن `COPR` میشه مشکل `SELinux `رو حل کرد:<br>
🔅 با تشکر ویژه از دوست عزیزمان [هدایت](http://hedayatvk.blogspot.ch/) به دلیل ایجاد بسته `  tor-exec-transport-selinux`.
```
dnf copr enable hedayat/tor-exec-transport-selinux
dnf install tor-exec-transport-selinux
```
<br>
### 🔅 Bridge Relays
<p dir="rtl">
🔅 منظور از پل چیه؟<br> در ویدئو و اول گفته‌ها، گفتیم که که ادرس سرورها برای ما ارسال میشه، پس در دسترس شرکت های ارائه دهنده اینترنت `ISP` یا حتی حکومت‌ها هم قرار میگیره و راحت میتونن دسترسی شما رو به شبکه تور مسدود کنند.<br> برای همین شما از پل استفاده می‌کنید. یکسری از سرورها `گره‌ها`ی تور هستند که در دسترس همه قرار نمی‌گیرند و شما می‌تونید با ارسال ایمیل به [این ادرس](mailto:bridges@torproject.org)، ادرس این سرور‌ها `پل‌ها` رو دریافت کنید. یا این‌که به [این آدرس](https://bridges.torproject.org) رجوع کنید و ادرس پل‌های جدید رو از داخل سایت اصلی `تور` کپی کنید . البته توجه کنید که این وب سایت فیلتر است!
### 🔅 Bridge Relays Configuration 
> 
<p dir="rtl">
برای اعمال پل‌ها، می‌تونید از دو روش استفاده کنید!<br>
با اطلاع‌رسانی هدایت عزیز، نسخه جدید `obfs4` که به همت ایشون وارد مخازن فدورا شده بود، جدیدا به همراه یکسری پل منتشر شده، و دیگه لازم نیست مراحل ارسال ایمیل ویا کپی پل ها از سایت رو که مربوط به روش دوم اتصال به شبکه تور در این مقاله است رو انجام بدید.<br>
پس این روش به عنوان روش اول انتخاب شد، اما اگر جواب نداد شما مجبور به استفاده از روش دوم هستید!


#### 🔅 Use obfs4.torrc
> 
<p dir="rtl">
دقت کنید که در اول آموزش شما مجبور شدید تا سرویس `tor.service`رو فعال کنید.<br>
اما سرویس  `tor@obfs4` نباید همزمان با سرویس `tor.service` فعال باشد!
پس با دستور
 ```
sudo systemctl stop tor.service
 ``` 
<p dir="rtl">سرویس تور رو جهت فعال کردن سرویس `tor@obfs4` غیرفعال کنید.







<p dir="rtl">
**روش اول**<br>
روش اول این‌که از دایرکتوری تور در آدرس `/etc/tor/` یک لیست‌فایل با کامند محبوب `ls`با دسترسی ریشه یا `root` بگیرید! و مطمئن شوید که فایل `obfs4.torrc` موجود است.

```
sudo ls /etc/tor/
```
![img](/img/obfs4.torrc.png "obfs4.torrc")
<p dir="rtl">
خب مشاهده‌گر هستید که فایل موجود است.پس با دستور زیر از صحت وجود پل‌ها در فایل هم مطمئن شوید:
```
sudo nano /etc/tor/obfs4.torrc
```
>
<p dir="rtl">
با نگه داشتن کلید‌های ` CTRL + V` به اخر لیست در فایل برید!<br>



![img](/img/obfs4.png "obfs4.torrc")
<p dir="rtl">
پل‌ها موجوده! بعد از این کافیه تا با دستور زیر سرویس `tor@obfs4` رو فعال کنید: 
```
sudo systemctl start tor@obfs4
```
<p dir="rtl">
دستور زیر رو جهت اطلاع از وضعیت اتصال وارد کنید : 
```
systemctl status tor@obfs4
```
<p dir="rtl">
کمی صبر کنید  تا به ۱۰۰٪ برسه و وضعیت رو با چند بار `systemctl status tor@obfs4` زدن بررسی کنید(برای خارج شدن از این وضعیت کلید` q `یا`CTRL + C `رو بزنید.)،<br> اگه به ٪۱۰۰ نرسید چندین باز با دستور `systemctl restart tor@obfs4` سرویس رو restart کنید و دوباره وضعیت رو چک کنید.<br>
اگه ۱۰۰٪ شد به مرحله ` Firefox Proxy Configuration` برای پیکربندی فایرفاکس برید!<br>در غیر این صورت از روش دوم استفاده کنید.

#### 🔅 Use torrc
> 
<p dir="rtl">
دقت کنید که در روش اول شما مجبور شدید تا سرویس `tor@obfs4`رو فعال کنید.<br>
اما سرویس  `tor@obfs4` نباید همزمان با سرویس `tor.service` فعال باشد!
پس با دستور
 ```
sudo systemctl stop tor@obfs4
 ``` 
<p dir="rtl">سرویس `tor@obfs4` رو جهت فعال کردن سرویس `tor.service` غیرفعال کنید.

<p dir="rtl">
**روش دوم** <br>
کافیه به ادرس زیر برید و فایل `torrc` رو پیکربندی کنید: 

```
sudo nano /etc/tor/torrc
```
<p dir="rtl">
 و با نگه داشتن کلید های `CTRL + V ` به اخر لیست در فایل برید و در اخر این چند خط رو اضافه کنید:
```
UseBridges 1

Bridge YOUR-OBFS4-BRIDGE
ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy
```
> 
<p dir="rtl">
و بجای این ` YOUR-OBFS4-BRIDGE ` پلهای خودتون رو طبق اسکرین شات زیر اضافه کنید.<br>



![img](/img/torrc-config.png "Torrc Config")
<p dir="rtl">
دوباره دستورات زیر رو وارد کنید : 
```
sudo systemctl restart tor.service
systemctl status tor.service
```
<p dir="rtl">
کمی صبر کنید  تا به ۱۰۰٪ برسه و وضعیت رو با چند بار `systemctl status tor.service` زدن بررسی کنید، اگه به ٪۱۰۰ نرسید پل های دیگه رو امتحان کنید.
### 🔅 Firefox Proxy Configuration
<p dir="rtl">
برای تنظیم فایرفاکس به منو برید، بعد به `Prefrences` بعد به `Advanced` بعد به `Network` و در قسمت `Connection ` روی `Settings ` کلیک کنید و مثل عکس زیر تنظیمات رو انجام بدید:
![img](/img/Firefox.png "Firefox Configure")
<p dir="rtl">
از تور برروی فایرفاکس لذت ببرید. 
###  🔅 Terminal Proxy with TOR
<p dir="rtl">
برای پراکسی کردن ترمینال هم قبل از دستورتون از `torsocks `  استفاده کنید:
```
torsocks YOUR-COMMAND
```
_____
<div id="graphcomment"></div>

















