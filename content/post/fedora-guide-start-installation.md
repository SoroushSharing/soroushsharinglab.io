---
title: "Fedora Guide"
subtitle: "#1 Start Installation"
date: 2018-01-03T12:48:04+03:30
tags: ["fedora", "installation", "linux", "Gnu/linux", "لینوکس", "فدورا", "گنولینوکس","نصب"]
bigimg: [{src: "/img/fedoracover.jpg", desc: "Get Fedora"}, {src: "/img/images.duckduckgo.com.png", desc: "Fedora GnuLinux"}]
---
<p dir="rtl">  
🔅 توزیع محبوب فدورا که از توزیع های متفاوت گنولینوکسه، از نظر بیشتر کاربران گنو لینوکس در ایران و جهان از لحاظ  امنیت ،  پایداری  و بروز بودن جزء چند توزیع معروفه و توسط گروه  رد هت پشتیبانی میشه.<br>

 🔅فدورا تلاش داره با ارائه یک سیستم عامل مبتنی بر یونیکس و برگرفته از هسته لینوکس کاربران سایر سیستم عامل هارو یکی پس از دیگری به آزادی دعوت و نهایت کاربر پسند بودن رو از آن خودش بکنه.
</p>

<!--more-->

### 🔅 Start Downloading 
<p dir="rtl">  
🔅 در ابتدا برای نصب فدورا، [طعم ایستگاه کاری رو دانلود کنید.](https://getfedora.org/)<br> 
 اگه کاربر ویندوز هستید لازمه تا فایل دانلود شده که با پسوند `iso.` هست رو، روی یه فلش بوتیبل کنید که میتونید از نرم افزار `RUFUS` استفاده کنید .<br> 
____
### 🔅 Start Installation
<p dir="rtl">  
در اولین قدم سیستم عامل به صورت زنده راه اندازی میشه!
![img](/img/Screenshot from 2018-01-03 12-06-50.png "Booting live OS")
#### ! `Test this media & start Fedora-Workstation-live`

### 🔅  Welcome to Fedora `Live`

<p dir="rtl">  
خب الان فدورا به صورت زنده ‍`Live` راه اندازی شده و آماده نصب سخت افزاریه.
![img](/img/Screenshot from 2018-01-03 12-07-43.png "ّInstalling On Storage")
#### ! `Install to Hard Drive`

<p dir="rtl">  
در این مرحله از نصب، زبان نصب سیستم عامل رو انتخاب کنید. <br> دقت کنید که در اینجا فقط زبان نصاب فدورا `Anaconda` رو تغییر میدید، ن‍ه زبان کل سیستمو .<br> 
![img](/img/Screenshot from 2018-01-03 03-39-03.png "Choose Language to Install Fedora ")
#### ! `Continue`
### 🔅  Installation Summary

<p dir="rtl">  
ساعت و تاریخ رو براساس موقعیت جغرافیایی انتخاب کنید .
![img](/img/Screenshot from 2018-01-03 03-38-20.png  "Time & Date")
#### ! `Time & Date`

### 🔅  Installation Destination
<p dir="rtl">  
در این قسمت از پروسه نصب که مهم ترین بخش است، شما باید هارد دیسک یا حافظه `SSD`  رو انتخاب کنید و دربخش ` Storage Configuration`  گزینه `Custom` رو انتخاب کنید تا بتونین فضای سیستم عامل رو بصورت دستی و شخصا پارتیشن بندی کنید.
![img](/img/Screenshot from 2018-01-03 03-39-23.png  "Select Device & Storage Configuration")
#### ! `1.Select Device: your Hard Disk or SSD 2.Set Storage Configuration: Custom 3.Done `

### 🔅  Manual Partitioning  
<p dir="rtl">  
خب نوبت به طراحی و برنامه ریزی برای نوع پارتیشن بندی و اختصاص فضا به هر دایرکتوری رسیده. <br>
پس بر روی کلید `+`  کلیک کنید.<br> 
ببینید الان فضای کلی ما `50` گیگه، شاید بهترین راه برای این مقدار از فضا این باشه تا دایرکتوری ‍`Root`  رو جدا از دایرکتوری `home/ ` ایجاد کنیم، این روش یک اصل امنیتی هم هست چرا؟!<br> چون جدا سازی دایرکتوری `home/` از `root ` جداسازی فایل های شخصی از  مسیر اصلی سیستم عامله.<br>دایرکتوری `home/` فضایی برای فایل هایی است که کاربر  ایجاد میکنه و هرزمان که برای سیستم عامل مشکل بیاد میتونیه فقط دایرکتوری ریشه رو حذف و دوباره ایجاد کند. 
![img](/img/Screenshot from 2018-01-03 03-39-30.png  "Manual Partitioning")
#### ! `Set Partitioning scheme: Standard Partition `


<p dir="rtl">  
در قدم اول دایرکتوری `/` = `Root`  رو با فضای `  15GiB ` میسازیم.<br> 
باز هم دقت کنید فقط زمانی که فضای دایرکتوری `Root ` از دایرکتوری `home/` جدا باشد،میتونین این حجم از فضا رو درنظر بگیرید.درغیر این صورت،حتما فضای خیلی بیشتری به دایرکتوری `Root` اختصاص بدهید.
![img](/img/Screenshot from 2018-01-03 03-40-05.png  "Manual Partitioning")
#### ! Adding `Root` Directory: `/` Mount Point.


<p dir="rtl">  
ایجاد `boot/` .
![img](/img/Screenshot from 2018-01-03 03-40-30.png  "Manual Partitioning")
#### ! Adding `/boot`  or `/bootefi` Mount Point.

<p dir="rtl">  
ایجاد `home/`، اگر میخواید دایرکتوری `home/` از ریشه `/` جدا باشد...
![img](/img/Screenshot from 2018-01-03 03-40-47.png  "Manual Partitioning")
#### ! Adding `/home` Mount Point.

<p dir="rtl">  
ایجاد `swap/`،  فضایی که شما به اسم `swap/` تعریف می‍کنین همان حافظه مجازی است.<br>این حافظه باید نصف مقدار رم اصلی شما تعریف بشه.
![img](/img/Screenshot from 2018-01-03 03-41-07.png  "Manual Partitioning")
#### ! Adding `/swap` Mount Point 

### 🔅 Summary of Changes
<p dir="rtl">  
خلاصه ای از تغییراتی که دادید اما هنوز برروی هارد دیسک اعمال نشده پس `Accept Changes `  رو بزنین. 
![img](/img/Screenshot from 2018-01-03 03-42-48.png  "Summary of Changes")
#### ! `Accept Changes `

### 🔅 Configuration
<p dir="rtl">  
در این مرحله تنظیمات اولیه کاربر رو انجام میدید.
در این مرحله رمز کاربر ارشد یا `Root`  را تعریف کنید.
توجه کنید این رمز رو نباید فراموش کنید.
![img](/img/Screenshot from 2018-01-03 03-43-07.png  "Configuration")
#### ! ‍`Root Password`


<p dir="rtl">  
در این مرحله حتما کاربر جدید رو بسازید و تیک این گزینه `Make this user administrator ` <br>رو هم بزنید.
 به دلایل امنیتی اینکارو حتما انجام بدید تا استاندارد هارو رعایت کرده باشید.
![img](/img/Screenshot from 2018-01-03 03-43-33.png  "Root Password")
#### ! `Create User & Enable checkbox to make this user ADMINISTRATOR `

<p dir="rtl">  
مراحل نصب به پایان رسید. 
![img](/img/Screenshot from 2018-01-03 19-23-24.png  "Create User")
#### ! `Fedora is now successfully installed `

### 🔅 Gnome Desktop
<p dir="rtl">  
برنامه ترمینال رو پیدا و اجرا کنید.
![img](/img/Screenshot from 2018-01-03 19-23-49.png  "Gnome Desktop")
#### ! `Search Terminal  `

### 🔅 Terminal 
<p dir="rtl">  
سیستم رو `reboot` بکنید و از فدورا لذت ببرید.
![img](/img/Screenshot from 2018-01-03 19-24-03.png  " Terminal ")
#### ! `Reboot`


### 🔅 Start Fedora Gnu/Linux 
<p dir="rtl">  

![img](/img/Screenshot from 2018-01-03 19-24-32.png  "Start Fedora Gnu/Linux ")
#### ! `Fedora Workstation`



____
##### 🔅 Read next `Fedora GNU/Linux` articles step `#2` [First Configuration](https://soroushsharing.gitlab.io/post/fedora-guide-configuration/) & step `#3` [TOR Configuration](https://soroushsharing.gitlab.io/post/fedora-guide-tor/).
____
<div id="graphcomment"></div>


